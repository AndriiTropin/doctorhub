package com.is_staff.doctorhub.helpers;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.adapters.MemberAdapter;
import com.is_staff.doctorhub.container.Conference;
import com.is_staff.doctorhub.container.User;

import java.util.List;

/**
 * Created by Andrii Tropin on 05.03.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class SwipeManagerMember {

    private final String node;
    private Paint p = new Paint();
    private Context context;
    private DatabaseReference mFirebaseDatabaseReference;
    private Conference conferences;
    private CoordinatorLayout coordinatorLayout;

    public SwipeManagerMember(Context context, DatabaseReference mFirebaseDatabaseReference, String node) {
        this.context = context;
        this.mFirebaseDatabaseReference = mFirebaseDatabaseReference;
        this.node = node;
        Activity activity = (Activity) context;
        coordinatorLayout = (CoordinatorLayout) activity.findViewById(R.id.list_member_layout);
    }

    public void initSwipe(final MemberAdapter adapter, RecyclerView recyclerView, final List<User> userArrayList) {

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                final int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    mFirebaseDatabaseReference.child("conference_list").child(node).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            conferences = dataSnapshot.getValue(Conference.class);
                            mFirebaseDatabaseReference.child("mail").child(userArrayList.get(position).getId()).child(node).setValue(conferences);
                            userArrayList.remove(position);
                            Notice.showSnack(coordinatorLayout,"Приглашение отправлено");
                            //FIXME пофиксить хардкод
                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if (dX > 0) {
                        p.setColor(context.getResources().getColor(R.color.delete));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_close_white_24dp);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    } else {
                        p.setColor(context.getResources().getColor(R.color.accept));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_check_white_24dp);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

}
