package com.is_staff.doctorhub.helpers;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by Andrii Tropin on 5/21/2017.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class DayViewCustomDecorator implements DayViewDecorator {
    private int color;
    private HashSet<CalendarDay> dates;

    public DayViewCustomDecorator(int color, Collection<CalendarDay> dates) {
        this.color = color;
        this.dates = new HashSet<>(dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay calendarDay) {
        return dates.contains(calendarDay);
    }

    @Override
    public void decorate(DayViewFacade dayViewFacade) {

        dayViewFacade.addSpan(new DotSpan(10,color));

    }
}
