package com.is_staff.doctorhub.helpers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.is_staff.doctorhub.activities.EventDetailActivity;

/**
 * Created by Andrii Tropin on 5/21/2017.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {


        String txtShort = intent.getStringExtra("txtShort");
        String txtLong = intent.getStringExtra("txtLong");
        int id = intent.getIntExtra("id", 0);

        Intent notificationIntent = new Intent(context, EventDetailActivity.class);
        //notificationIntent.putExtra("msg", msg);
        notificationIntent.putExtra("txtLong", txtLong);

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        //Resources res = context.getResources();
        Notification.Builder builder = new Notification.Builder(context);
        // Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ojicon);

        builder.setContentIntent(contentIntent)
                // .setSmallIcon(R.drawable.ojdr)
                // .setLargeIcon(largeIcon)
                // .setTicker(context.getString(R.string.oj))
                .setWhen(System.currentTimeMillis()) // java.lang.System.currentTimeMillis()
                .setAutoCancel(true)
                // .setContentTitle(context.getString(R.string.oj))
                // .setContentText(res.getString(R.string.notifytext))
                .setContentText(txtShort); //

        Notification n = builder.getNotification();
        n.defaults = Notification.DEFAULT_SOUND |
                Notification.DEFAULT_VIBRATE;

        nm.notify(id, n);
    }
}

