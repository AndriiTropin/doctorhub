package com.is_staff.doctorhub.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.google.firebase.database.DatabaseReference;
import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.adapters.TopicsAdapterAdmin;
import com.is_staff.doctorhub.container.Topic;

import java.util.List;

/**
 * Created by Andrii Tropin on 05.03.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class SwipeManagerTopics {

    private Paint p = new Paint();
    private Context context;
    private DatabaseReference mFirebaseDatabaseReference;

    public SwipeManagerTopics(Context context, DatabaseReference mFirebaseDatabaseReference) {
        this.context = context;
        this.mFirebaseDatabaseReference = mFirebaseDatabaseReference;
    }

    public void initSwipe(final TopicsAdapterAdmin adapter, RecyclerView recyclerView, final List<Topic> topicArrayList) {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    // add item
                    mFirebaseDatabaseReference.child("topics").child(topicArrayList.get(position).getConference()).child(topicArrayList.get(position).getId()).setValue(topicArrayList.get(position));
                    mFirebaseDatabaseReference.child("topics_temp").child(topicArrayList.get(position).getId()).removeValue();
                    topicArrayList.remove(position);
                    adapter.notifyDataSetChanged();
                } else {
                    // delete item
                    mFirebaseDatabaseReference.child("topics_temp").child(topicArrayList.get(position).getId()).removeValue();
                    topicArrayList.remove(position);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if (dX > 0) {
                        p.setColor(context.getResources().getColor(R.color.delete));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_close_white_24dp);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    } else {
                        p.setColor(context.getResources().getColor(R.color.accept));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_check_white_24dp);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

}
