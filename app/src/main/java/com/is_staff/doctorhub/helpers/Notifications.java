package com.is_staff.doctorhub.helpers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Andrii Tropin on 5/21/2017.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class Notifications {
    private Context mContext;

    public Notifications(Context mContext) {
        this.mContext = mContext;
    }

    public void registerNotif1(int tempId, String dateTime, String txtShort, String txtLong) {

        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");

        Intent myIntent = new Intent(mContext, AlarmReceiver.class);

        myIntent.putExtra("txtShort", txtShort);
        myIntent.putExtra("txtLong", txtLong);
        myIntent.putExtra("id", tempId);

        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, tempId, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        try {
            Date dtIn = inFormat.parse(dateTime);
            // long time = calendar.getTimeInMillis();
            long time = dtIn.getTime();
            alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
            Log.v("My", "" + alarmManager);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}

