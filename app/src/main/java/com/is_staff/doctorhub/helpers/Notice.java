package com.is_staff.doctorhub.helpers;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;

/**
 * Created by Andrii Tropin on 04.04.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class Notice {
    public static void showSnack(CoordinatorLayout coordinatorLayout, String s){
        Snackbar.make(coordinatorLayout,s, Snackbar.LENGTH_SHORT).show();
    }
}
