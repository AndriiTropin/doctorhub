package com.is_staff.doctorhub.interfaces;

/**
 * Created by Andrii Tropin on 26.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public interface OnNewTopicCreateListener {
    void onNewTopic(String newtopic);
}
