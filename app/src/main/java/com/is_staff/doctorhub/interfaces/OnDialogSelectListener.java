package com.is_staff.doctorhub.interfaces;

/**
 * Created by Andrii Tropin on 19.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public interface OnDialogSelectListener {

    void onSelect(int position);
}
