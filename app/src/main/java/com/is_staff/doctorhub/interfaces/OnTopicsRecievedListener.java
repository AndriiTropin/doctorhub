package com.is_staff.doctorhub.interfaces;

import com.is_staff.doctorhub.container.Topic;

import java.util.ArrayList;

/**
 * Created by Andrii Tropin on 26.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public interface OnTopicsRecievedListener {
    void onRecieved(ArrayList<Topic> listTopics);
}
