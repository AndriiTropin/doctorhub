package com.is_staff.doctorhub.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.fragments.AdminConferenceFragment;

/**
 * Created by Andrii Tropin on 26.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class AdminTopicsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_topics);

        AdminConferenceFragment fragmentConference = new AdminConferenceFragment();
        FragmentManager fm = getSupportFragmentManager();

        fm.beginTransaction().replace(R.id.activity_admin_topics_fl, fragmentConference).commit();
        ImageView imageView = (ImageView) findViewById(R.id.back_btn);
        imageView.setOnClickListener(v -> AdminTopicsActivity.super.onBackPressed());


    }
}
