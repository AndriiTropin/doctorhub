package com.is_staff.doctorhub.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.adapters.ViewPagerAdapter;
import com.is_staff.doctorhub.container.Conference;
import com.is_staff.doctorhub.container.Topic;
import com.is_staff.doctorhub.fragments.DetailFragment;
import com.is_staff.doctorhub.fragments.TopicsFragment;
import com.is_staff.doctorhub.helpers.Notice;
import com.is_staff.doctorhub.interfaces.OnNewTopicCreateListener;
import com.is_staff.doctorhub.interfaces.OnTopicsRecievedListener;

import java.util.ArrayList;

/**
 * Created by Andrii Tropin on  12.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class DetailConferenceActivity extends AppCompatActivity implements OnNewTopicCreateListener {

    private Conference model;
    private String ConferenceId;
    private final ArrayList<Topic> topicList = new ArrayList<>();
    private OnTopicsRecievedListener listener;
    private DatabaseReference mFirebaseDatabaseReference;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_conference);


        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.detail_layout);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout);
        ViewPager viewPager = (ViewPager) findViewById(R.id.fragment_period_viewpager);


        ImageView imageViewBackBtn = (ImageView) findViewById(R.id.back_btn);
        imageViewBackBtn.setOnClickListener(v -> DetailConferenceActivity.super.onBackPressed());

        TextView titleTxt = (TextView) findViewById(R.id.conference_title);
        TextView placeTxt = (TextView) findViewById(R.id.conference_place);
        TextView dateTxt = (TextView) findViewById(R.id.conference_date_time);


        model = (Conference) getIntent().getSerializableExtra("model");
        ConferenceId =  getIntent().getStringExtra("ConferenceId");

        titleTxt.setText(model.getTitle());
        placeTxt.setText(model.getPlace());
        dateTxt.setText(model.getDate());

        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseDatabaseReference.child("topics").child(ConferenceId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    topicList.add(dataSnapshot1.getValue(Topic.class));
                }
                listener.onRecieved(topicList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        tabLayout.setupWithViewPager(viewPager);
        setUpViewPager(viewPager);
    }

    private void setUpViewPager(ViewPager viewPager) {
        ViewPagerAdapter pageAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);
        Fragment fragmentDetail = new DetailFragment();
        Fragment fragmentTopic = new TopicsFragment();
        listener = (OnTopicsRecievedListener) fragmentTopic;
        Bundle bundle = new Bundle();
        bundle.putSerializable("model", model);

        fragmentDetail.setArguments(bundle);
        fragmentTopic.setArguments(bundle);
        pageAdapter.addFragment(fragmentDetail);
        pageAdapter.addFragment(fragmentTopic);
        viewPager.setAdapter(pageAdapter);
    }


    @Override
    public void onNewTopic(String newtopic) {

        Notice.showSnack(coordinatorLayout,"Тезис отправлен Администратору на рассмотрение");
        //FIXME вынести в string
        addNewTopicToFirebase(newtopic);
    }

    private void addNewTopicToFirebase(String newtopic) {
        String displayName = FirebaseAuth.getInstance().getCurrentUser().getDisplayName(); //FIXME getDisplayName()->NulllPointerExeption
        Topic topic = new Topic();
        topic.setTitle(newtopic);
        topic.setNameAuthor(displayName);
        topic.setConference(ConferenceId);
        mFirebaseDatabaseReference.child("topics_temp").push().setValue(topic);//FIXME Hardcode
    }
}