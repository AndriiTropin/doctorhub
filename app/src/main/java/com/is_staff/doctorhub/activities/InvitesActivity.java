package com.is_staff.doctorhub.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.adapters.ConferencesViewHolder;
import com.is_staff.doctorhub.container.Conference;
import com.is_staff.doctorhub.helpers.SwipeManagerMail;

/**
 * Created by Andrii Tropin on 12.03.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class InvitesActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invites);
        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
        DatabaseReference mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.activity_mail_recycerview);
        FirebaseRecyclerAdapter firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Conference, ConferencesViewHolder>(
                Conference.class,
                R.layout.activity_invites_item, //FIXME Hardcode - переименовать
                ConferencesViewHolder.class,
                mFirebaseDatabaseReference.child("mail").child(mFirebaseUser.getUid())) { //FIXME Hardcode

            @Override
            protected void populateViewHolder(ConferencesViewHolder viewHolder, Conference model, int position) {
                viewHolder.titleTxt.setText(model.getTitle());
                viewHolder.placeTxt.setText(model.getPlace());
                viewHolder.dateTxt.setText(model.getDate());
            }
        };

        SwipeManagerMail swipeManagerMail = new SwipeManagerMail(this, mFirebaseDatabaseReference, mFirebaseUser.getUid());
        swipeManagerMail.initSwipe(firebaseRecyclerAdapter, mRecyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(firebaseRecyclerAdapter);

        ImageView imageViewBackBtn = (ImageView) findViewById(R.id.back_btn);
        imageViewBackBtn.setOnClickListener(v -> InvitesActivity.super.onBackPressed());
    }
}
