package com.is_staff.doctorhub.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.container.Conference;
import com.is_staff.doctorhub.helpers.Notifications;

public class EventDetailActivity extends AppCompatActivity {

    private TextView titleTxt;
    private TextView placeTxt;
    private TextView detailTxt;
    private TextView dateTxt;
    private String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_detail);
        titleTxt = (TextView) findViewById(R.id.conference_title_modify);
        placeTxt = (TextView) findViewById(R.id.conference_place_modify);
        detailTxt = (TextView) findViewById(R.id.conference_detail_modify);
        dateTxt = (TextView) findViewById(R.id.conference_date_time_modify);
        final String ConferenceId = getIntent().getStringExtra("ConferenceId");
        DatabaseReference firebaseDatabaseReference = FirebaseDatabase.getInstance().getReference().child("conference_list").child(ConferenceId);

        firebaseDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    Conference conference = dataSnapshot.getValue(Conference.class);
                    fillEventDetail(conference);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        Button reminder = (Button) findViewById(R.id.button_remainder);
        reminder.setOnClickListener(v -> new Notifications(EventDetailActivity.this).registerNotif1(ConferenceId.hashCode(), date, "short_message","long_message"));
    }

    private void fillEventDetail(Conference conference) {
        if (conference != null) {
            date = conference.getDate();
            titleTxt.setText(conference.getTitle());
            placeTxt.setText(conference.getPlace());
            detailTxt.setText(conference.getDetail());
            dateTxt.setText(conference.getDate());
        }
    }
}
