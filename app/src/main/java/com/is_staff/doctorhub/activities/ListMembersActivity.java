package com.is_staff.doctorhub.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.adapters.MemberAdapter;
import com.is_staff.doctorhub.container.User;
import com.is_staff.doctorhub.helpers.SwipeManagerMember;

import java.util.ArrayList;

/**
 * Created by Andrii Tropin on 05.03.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class ListMembersActivity extends AppCompatActivity {
    private ArrayList<User> userArrayList = new ArrayList<>();
    private MemberAdapter memberAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_members);

        ImageView imageViewBackBtn = (ImageView) findViewById(R.id.back_btn);
        imageViewBackBtn.setOnClickListener(v -> ListMembersActivity.super.onBackPressed());

        String userId = getIntent().getStringExtra("userId");
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.activity_list_members_recycerview);
        memberAdapter = new MemberAdapter(this, userArrayList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(memberAdapter);

        DatabaseReference mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        SwipeManagerMember swipeManagerMember = new SwipeManagerMember(this, mFirebaseDatabaseReference, userId);
        swipeManagerMember.initSwipe(memberAdapter, mRecyclerView, userArrayList);

        mFirebaseDatabaseReference.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot tkey : dataSnapshot.getChildren()) {
                    User user = tkey.getValue(User.class);
                    String s = tkey.getKey();
                    user.setId(s);
                    userArrayList.add(user);
                }
                memberAdapter.notifyDataSetChanged();
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


    }
}
