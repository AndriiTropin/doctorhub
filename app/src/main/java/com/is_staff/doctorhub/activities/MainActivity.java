package com.is_staff.doctorhub.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.adapters.ConferencesViewHolder;
import com.is_staff.doctorhub.container.Conference;
import com.is_staff.doctorhub.container.User;
import com.is_staff.doctorhub.fragments.DialogCalendar;
import com.is_staff.doctorhub.fragments.DialogConferenceModifyFragment;
import com.is_staff.doctorhub.helpers.Notice;
import com.is_staff.doctorhub.interfaces.OnDialogSelectListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends AppCompatActivity implements OnDialogSelectListener, NavigationView.OnNavigationItemSelectedListener {
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private String photoUrl;
    private GoogleApiClient googleApiClient;
    private DatabaseReference ConferenceReference;
    private Conference modelForModify;
    private DatabaseReference databaseUserReference;
    private DatabaseReference mFirebaseDatabaseReference;
    private boolean adminCheck = false;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        if (mFirebaseUser == null) {
            startActivity(new Intent(this, AuthActivity.class));
            finish();
            return;
        } else {

            if (mFirebaseUser.getPhotoUrl() != null) {
                photoUrl = mFirebaseUser.getPhotoUrl().toString();
            }
        }
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.activity_main);
        Notice.showSnack(coordinatorLayout, "you are logged in!");
        //FIXME hardcode
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, connectionResult -> Log.e("my_app", "onConnectionFailed: "))
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);
        View headerLayout = navigationView.getHeaderView(0);

        ImageView driverBtn = (ImageView) findViewById(R.id.back_btn);
        driverBtn.setImageResource(R.drawable.ic_menu_white_24dp);
        driverBtn.setOnClickListener(v -> drawer.openDrawer(Gravity.START));


        ImageView imgCalendar = (ImageView) findViewById(R.id.calendar);
        imgCalendar.setOnClickListener(v -> new DialogCalendar().show(getSupportFragmentManager(), ""));

        CircleImageView photoUser = (CircleImageView) headerLayout.findViewById(R.id.header_photo);
        TextView name = (TextView) headerLayout.findViewById(R.id.header_name);
        TextView email = (TextView) headerLayout.findViewById(R.id.header_email);

        Picasso.with(this) //передаем контекст приложения
                .load(mFirebaseUser.getPhotoUrl()) //адрес изображения
                .into(photoUser); //ссылка на ImageView

        name.setText(mFirebaseUser.getDisplayName());
        email.setText(mFirebaseUser.getEmail());


        databaseUserReference = FirebaseDatabase.getInstance().getReference().child("members_list_conference").child(mFirebaseUser.getUid());
        databaseUserReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {

                    SharedPreferences sharedPreferences;
                    sharedPreferences = getPreferences(MODE_PRIVATE);
                    SharedPreferences.Editor ed = sharedPreferences.edit();
                    for (DataSnapshot tkey : dataSnapshot.getChildren()) {
                        String a = tkey.getKey();
                        String b = tkey.getValue().toString();
                        ed.putString(a, b);
                    }
                    ed.apply();
                    Log.v("my_app", "nodeDate");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        databaseUserReference = FirebaseDatabase.getInstance().getReference().child("users").child(mFirebaseUser.getUid());
        databaseUserReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user == null) {
                        createUser(adminCheck);
                    } else {
                        adminCheck = user.isAdmin();
                        createUser(adminCheck);
                    }
                    createFloatButton();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        RecyclerView mMessageRecyclerView = (RecyclerView) findViewById(R.id.activity_main_recycerview);
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        FirebaseRecyclerAdapter mFirebaseAdapter = new FirebaseRecyclerAdapter<Conference, ConferencesViewHolder>(
                Conference.class,
                R.layout.activity_main_content_item,
                ConferencesViewHolder.class,
                mFirebaseDatabaseReference.child("conference_list")) { //FIXME Hardcode

            @Override
            public DatabaseReference getRef(int position) {
                return super.getRef(position);
            }

            @Override
            protected void populateViewHolder(ConferencesViewHolder viewHolder, final Conference model, final int position) {
                viewHolder.titleTxt.setText(model.getTitle());
                viewHolder.placeTxt.setText(model.getPlace());
                viewHolder.dateTxt.setText(model.getDate());
                viewHolder.conferenceItem.setOnClickListener(v -> {
                    Intent intent = new Intent(MainActivity.this, DetailConferenceActivity.class);
                    intent.putExtra("model", model);
                    intent.putExtra("ConferenceId", getRef(position).getKey());
                    startActivity(intent);
                });
                viewHolder.conferenceItem.setOnLongClickListener(v -> {
                    if (adminCheck) {
                        ConferenceReference = getRef(position);
                        modelForModify = model;
                        new DialogConferenceModifyFragment().show(getSupportFragmentManager(), "");
                    }
                    return false;
                });
            }
        };
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);
        mMessageRecyclerView.setAdapter(mFirebaseAdapter);
        mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 41:
                if (resultCode == RESULT_OK) {
                    Notice.showSnack(coordinatorLayout, "конференция добавлена");
                }
                //Fixme hardcode
                break;
            case 42:
                if (resultCode == RESULT_OK) {
                    Notice.showSnack(coordinatorLayout, "модификация успешна");
                }
                //Fixme hardcode
                break;
        }
    }

    private void createFloatButton() {
        FloatingActionButton floatbutton = (FloatingActionButton) findViewById(R.id.floatbuton);
        if (adminCheck) {
            floatbutton.setVisibility(View.VISIBLE);
            floatbutton.setOnClickListener(v -> {
                Intent intent = new Intent(MainActivity.this, DetailConferenceModifyActivity.class);
                startActivityForResult(intent, 41);
            });
        }
    }

    private void createUser(boolean setAdmin) {
        User user = new User();
        user.setName(mFirebaseUser.getDisplayName());
        user.setAdmin(setAdmin);
        user.setPhotoUrl(photoUrl);
        databaseUserReference.setValue(user);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


    @Override
    public void onSelect(int position) {
        switch (position) {
            case 0:
                //Delete
                ConferenceReference.setValue(null);
                mFirebaseDatabaseReference.child("topics").child(ConferenceReference.getKey()).setValue(null);
                Notice.showSnack(coordinatorLayout, "конференция удалена");
                //FIXME хардкод
                break;
            case 1:
                //Modify
                Intent intent = new Intent(MainActivity.this, DetailConferenceModifyActivity.class);
                intent.putExtra("model", modelForModify);
                intent.putExtra("ConferenceId", ConferenceReference.getKey());
                startActivityForResult(intent, 42);
                break;
            case 2:
                //Invite Doctors
                Intent intentMembers = new Intent(MainActivity.this, ListMembersActivity.class);
                intentMembers.putExtra("userId", ConferenceReference.getKey());
                startActivity(intentMembers);
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                mFirebaseAuth.signOut();
                Auth.GoogleSignInApi.signOut(googleApiClient);
                mFirebaseUser = null;
                startActivity(new Intent(this, AuthActivity.class));
                finish();
                break;
            case R.id.inbox_mail:
                if (adminCheck) {
                    startActivity(new Intent(this, AdminTopicsActivity.class));
                } else {
                    startActivity(new Intent(this, InvitesActivity.class));
                }
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
