package com.is_staff.doctorhub.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.container.Conference;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Andrii Tropin on 12.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class DetailConferenceModifyActivity extends AppCompatActivity {
    private TextView titleTxt;
    private TextView placeTxt;
    private TextView dateTxt;
    private TextView detailTxt;
    private String ConferenceId;
    private DatabaseReference databaseReference;
    private Calendar dateAndTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_detail_modify);

        ImageView imageViewBackBtn = (ImageView) findViewById(R.id.back_btn);
        imageViewBackBtn.setOnClickListener(v -> DetailConferenceModifyActivity.super.onBackPressed());

        titleTxt = (TextView) findViewById(R.id.conference_title_modify);
        placeTxt = (TextView) findViewById(R.id.conference_place_modify);
        detailTxt = (TextView) findViewById(R.id.conference_detail_modify);
        dateTxt = (TextView) findViewById(R.id.conference_date_time_modify);


        dateAndTime = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener d = (view, year, monthOfYear, dayOfMonth) -> {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, monthOfYear);
            dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialDateTime();
        };

        dateTxt.setOnClickListener(v -> new DatePickerDialog(DetailConferenceModifyActivity.this, d,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH))
                .show());


        Conference model = (Conference) getIntent().getSerializableExtra("model");
        if (model != null) {
            titleTxt.setText(model.getTitle());
            placeTxt.setText(model.getPlace());
            dateTxt.setText(model.getDate());
            detailTxt.setText(model.getDetail());
        }
        ConferenceId = getIntent().getStringExtra("ConferenceId");


        databaseReference = FirebaseDatabase.getInstance().getReference().child("conference_list");
        Button buttonSave = (Button) findViewById(R.id.button_save);
        buttonSave.setOnClickListener(v -> {
            Conference conferences = new Conference();
            conferences.setTitle(titleTxt.getText().toString());
            conferences.setPlace(placeTxt.getText().toString());
            conferences.setDate(dateTxt.getText().toString());
            conferences.setDetail(detailTxt.getText().toString());
            if (ConferenceId != null) {
                databaseReference.child(ConferenceId).setValue(conferences);
            } else {
                databaseReference.push().setValue(conferences);
            }
            setResult(RESULT_OK);
            finish();
        });
    }

    private void setInitialDateTime() {
        Date date = new Date();
        date.setTime(dateAndTime.getTimeInMillis());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        dateTxt.setText(simpleDateFormat.format(date));
    }
}
