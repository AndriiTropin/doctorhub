package com.is_staff.doctorhub.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.helpers.Notice;

/**
 * Created by Andrii Tropin on 29.01.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class AuthActivity extends AppCompatActivity {

    private FirebaseAuth mFirebaseAuth;
    private GoogleApiClient googleApiClient;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_layout);

        Button buttonAuth = (Button) findViewById(R.id.auth_layout_button);
        mFirebaseAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, connectionResult -> {
                    Notice.showSnack(coordinatorLayout,"Нет связи с Google");
                    //FIXME пофиксить хардкод
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                .build();

        buttonAuth.setOnClickListener(view -> {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
            startActivityForResult(signInIntent, 42);
        });

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.auth_layout);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 42) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount signInAccount = result.getSignInAccount();
                fireBaseGoogleAuth(signInAccount);
            } else {
                Notice.showSnack(coordinatorLayout,"Нет связи с интернетом");
                //FIXME пофиксить хардкод
            }
        }
    }

    private void fireBaseGoogleAuth(GoogleSignInAccount signInAccount) {
        AuthCredential credential = GoogleAuthProvider.getCredential(signInAccount.getIdToken(), null);
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        startActivity(new Intent(AuthActivity.this, MainActivity.class));
                        finish();
                    } else {
                        Notice.showSnack(coordinatorLayout,"Нет связи с сервером");
                        //FIXME пофиксить хардкод
                    }
                });
    }
}
