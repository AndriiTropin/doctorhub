package com.is_staff.doctorhub.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.container.Topic;

import java.util.ArrayList;

/**
 * Created by Andrii Tropin on 26.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class TopicsAdapter extends RecyclerView.Adapter<TopicsAdapter.MyViewHolder> {

    private ArrayList<Topic> topicList;
    private Context context;

    public TopicsAdapter(Context context, ArrayList<Topic> topicList) {
        this.topicList = topicList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.fragment_topics_item, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.fragmentCardviewAuthor.setText(topicList.get(position).getNameAuthor());
        holder.fragmentCardviewTopic.setText(topicList.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return topicList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView fragmentCardviewAuthor;
        TextView fragmentCardviewTopic;

        MyViewHolder(View itemView) {
            super(itemView);
            fragmentCardviewAuthor = (TextView) itemView.findViewById(R.id.fragment_cardview_author);
            fragmentCardviewTopic = (TextView) itemView.findViewById(R.id.fragment_cardview_topic);
        }
    }
}
