package com.is_staff.doctorhub.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.container.User;

import java.util.ArrayList;

/**
 * Created by Andrii Tropin on 05.03.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.MyViewHolder> {
    private ArrayList<User> userList;
    private Context context;

    public MemberAdapter(Context context, ArrayList<User> userList) {
        this.userList = userList;
        this.context = context;
    }

    @Override
    public MemberAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_list_members_item, parent, false);
        return new MemberAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MemberAdapter.MyViewHolder holder, int position) {
        holder.member.setText(userList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView member;

        MyViewHolder(View itemView) {
            super(itemView);
            member = (TextView) itemView.findViewById(R.id.member);
        }
    }

}
