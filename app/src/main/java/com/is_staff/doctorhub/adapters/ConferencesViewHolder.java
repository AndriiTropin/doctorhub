package com.is_staff.doctorhub.adapters;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.is_staff.doctorhub.R;

/**
 * Created by Andrii Tropin on 07.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class ConferencesViewHolder extends RecyclerView.ViewHolder {

    public TextView titleTxt;
    public TextView placeTxt;
    public TextView dateTxt;
    public CardView conferenceItem;

    public ConferencesViewHolder(View v) {
        super(v);
        titleTxt = (TextView) v.findViewById(R.id.cardview_title);
        placeTxt = (TextView) v.findViewById(R.id.cardview_place);
        dateTxt = (TextView) v.findViewById(R.id.cardview_date_time);
        conferenceItem = (CardView) v.findViewById(R.id.to_activity_main_cardview);
    }
}

