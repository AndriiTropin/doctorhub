package com.is_staff.doctorhub.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.container.Topic;

import java.util.ArrayList;

/**
 * Created by Andrii Tropin on 26.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class TopicsAdapterAdmin extends RecyclerView.Adapter<TopicsAdapterAdmin.MyViewHolder> {

    private ArrayList<Topic> topicList;
    private Context context;

    public TopicsAdapterAdmin(Context context, ArrayList<Topic> topicList) {
        this.topicList = topicList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_admin_topics_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.fragmentAdminCardviewAuthor.setText(topicList.get(position).getNameAuthor());
        holder.fragmentAdminCardviewTopic.setText(topicList.get(position).getTitle());
        holder.fragmentAdminCardviewConference.setText(topicList.get(position).getConference());

    }

    @Override
    public int getItemCount() {
        return topicList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView fragmentAdminCardviewAuthor;
        TextView fragmentAdminCardviewTopic;
        TextView fragmentAdminCardviewConference;

        MyViewHolder(View itemView) {
            super(itemView);
            fragmentAdminCardviewAuthor = (TextView) itemView.findViewById(R.id.fragment_admin_cardview_author);
            fragmentAdminCardviewTopic = (TextView) itemView.findViewById(R.id.fragment_admin_cardview_topic);
            fragmentAdminCardviewConference = (TextView) itemView.findViewById(R.id.fragment_admin_cardview_conference);
        }
    }
}
