package com.is_staff.doctorhub.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.is_staff.doctorhub.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrii Tropin on 12.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private String[] tab;

    private List<Fragment> fragmentList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        tab = context.getResources().getStringArray(R.array.viewpager);
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tab[position];
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }


    public void addFragment(Fragment fragment) {
        fragmentList.add(fragment);
    }
}
