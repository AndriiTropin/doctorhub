package com.is_staff.doctorhub.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.activities.EventDetailActivity;
import com.is_staff.doctorhub.container.ConferenceDate;
import com.is_staff.doctorhub.helpers.DayViewCustomDecorator;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Andrii Tropin on 04.04.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */
public class DialogCalendar extends DialogFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getDialog() != null) getDialog().setCanceledOnTouchOutside(true);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_calendar, null);
        final AlertDialog.Builder createProjectAlert = new AlertDialog.Builder(getActivity());
        createProjectAlert.setView(view);
        MaterialCalendarView materialCalendar = (MaterialCalendarView) view.findViewById(R.id.calendarView);
        materialCalendar.setSelectionColor(Color.parseColor("#00BCD4"));
        materialCalendar.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);

        final ArrayList<ConferenceDate> conferenceDates = new ArrayList<>();
        SharedPreferences sPref;
        sPref = getActivity().getPreferences(MODE_PRIVATE);
        Map<String, ?> map = sPref.getAll();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        final ArrayList<CalendarDay> listDays = new ArrayList<>();
        for (Map.Entry<String, ?> entry : map.entrySet()) {
            try {
                date = dateFormat.parse(entry.getValue().toString());
                CalendarDay calendarDay = CalendarDay.from(date);
                conferenceDates.add(new ConferenceDate(entry.getKey(), calendarDay));
                listDays.add(calendarDay);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        DayViewCustomDecorator dayViewDecorator = new DayViewCustomDecorator(R.color.delete, listDays);
        materialCalendar.addDecorator(dayViewDecorator);
        materialCalendar.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                if (listDays.contains(calendarDay)) {
                    for (ConferenceDate conferenceDate : conferenceDates) {
                        if (conferenceDate.getConferenceDate().equals(calendarDay)){
                            Intent intent = new Intent(getActivity(), EventDetailActivity.class);
                            intent.putExtra("ConferenceId", conferenceDate.getConferenceId());
                            startActivity(intent);
                        }
                    }
                }
            }
        });
        return createProjectAlert.create();
    }

}
