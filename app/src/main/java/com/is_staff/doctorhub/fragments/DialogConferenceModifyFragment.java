package com.is_staff.doctorhub.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.is_staff.doctorhub.interfaces.OnDialogSelectListener;

/**
 * Created by Andrii Tropin on 19.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class DialogConferenceModifyFragment extends DialogFragment {

    private OnDialogSelectListener onDialogSelectListener;

    public DialogConferenceModifyFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getDialog() != null) getDialog().setCanceledOnTouchOutside(true);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        String[] menu = {"Delete", "Modify", "Invite Doctors"};

        alertDialog.setItems(menu, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                onDialogSelectListener.onSelect(which);
            }
        });

        alertDialog.setCancelable(true);
        return alertDialog.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onDialogSelectListener = (OnDialogSelectListener) activity;
    }
}
