package com.is_staff.doctorhub.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.adapters.TopicsAdapterAdmin;
import com.is_staff.doctorhub.container.Topic;
import com.is_staff.doctorhub.helpers.SwipeManagerTopics;

import java.util.ArrayList;


public class AdminConferenceFragment extends Fragment {
    private ArrayList<Topic> topicArrayList = new ArrayList<>();
    private TopicsAdapterAdmin topicsAdapter;

    public AdminConferenceFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_admin_topics, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView mMessageRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_admin_rv);
        final DatabaseReference mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);
        topicsAdapter = new TopicsAdapterAdmin(getActivity(), topicArrayList);
        mMessageRecyclerView.setAdapter(topicsAdapter);

        SwipeManagerTopics swipeManager = new SwipeManagerTopics(getActivity(), mFirebaseDatabaseReference);
        swipeManager.initSwipe(topicsAdapter, mMessageRecyclerView, topicArrayList);

        mFirebaseDatabaseReference.child("topics_temp").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot tkey : dataSnapshot.getChildren()) {
                    Topic topic = tkey.getValue(Topic.class);
                    String s = tkey.getKey() + "";
                    topic.setId(s);
                    topicArrayList.add(topic);
                }
                topicsAdapter.notifyDataSetChanged();

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


    }
}
