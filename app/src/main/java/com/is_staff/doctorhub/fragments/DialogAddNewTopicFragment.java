package com.is_staff.doctorhub.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.interfaces.OnNewTopicCreateListener;

/**
 * Created by Andrii Tropin on 19.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class DialogAddNewTopicFragment extends DialogFragment {

    private OnNewTopicCreateListener onNewTopicCreate;

    public DialogAddNewTopicFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getDialog() != null) getDialog().setCanceledOnTouchOutside(true);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_add_topic, null);
        AlertDialog.Builder createProjectAlert = new AlertDialog.Builder(getActivity());
        createProjectAlert.setTitle("Idea for topics"); //TODO Hardcode

        createProjectAlert.setView(view)
                .setPositiveButton("Send", (dialog, id) -> {
                    EditText topicTxt = (EditText) view.findViewById(R.id.project_topic);
                    onNewTopicCreate.onNewTopic(topicTxt.getText().toString());

                })
                .setNegativeButton("Cancel", (dialog, id) -> {
                });
        return createProjectAlert.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onNewTopicCreate = (OnNewTopicCreateListener) activity;
    }
}
