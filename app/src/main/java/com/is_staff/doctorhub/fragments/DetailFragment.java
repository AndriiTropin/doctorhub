package com.is_staff.doctorhub.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.container.Conference;

public class DetailFragment extends Fragment {


    private Conference conference;

    public DetailFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.conference = (Conference) getArguments().getSerializable("model");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView textViewDetail = (TextView) view.findViewById(R.id.fragment_detail_textView);
        textViewDetail.setText(conference.getDetail());
    }
}
