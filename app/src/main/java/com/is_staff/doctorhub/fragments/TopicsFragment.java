package com.is_staff.doctorhub.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.is_staff.doctorhub.R;
import com.is_staff.doctorhub.adapters.TopicsAdapter;
import com.is_staff.doctorhub.container.Topic;
import com.is_staff.doctorhub.interfaces.OnTopicsRecievedListener;

import java.util.ArrayList;

public class TopicsFragment extends Fragment implements OnTopicsRecievedListener {


    private ArrayList<Topic> topicList = new ArrayList<>();
    private TopicsAdapter topicsAdapter;

    public TopicsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_topics, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView mMessageRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_topics_rv);
        topicsAdapter = new TopicsAdapter(getActivity(), topicList);
        final LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);
        mMessageRecyclerView.setAdapter(topicsAdapter);
        FloatingActionButton floatbutton = (FloatingActionButton) view.findViewById(R.id.fragment_topics_floatbuton);
        floatbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogAddNewTopicFragment dialog = new DialogAddNewTopicFragment();
                dialog.show(getFragmentManager(), "");
            }
        });


    }

    @Override
    public void onRecieved(ArrayList<Topic> listTopics) {

        topicList.addAll(listTopics);
        if (topicsAdapter != null) topicsAdapter.notifyDataSetChanged();
    }
}
