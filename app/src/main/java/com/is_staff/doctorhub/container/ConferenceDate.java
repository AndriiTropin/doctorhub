package com.is_staff.doctorhub.container;

import com.prolificinteractive.materialcalendarview.CalendarDay;

/**
 * Created by Andrii Tropin on 5/21/2017.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class ConferenceDate {

    private String mConferenceId;
    private CalendarDay mConferenceDate;

    public ConferenceDate(String conferenceId, CalendarDay conferenceDate) {
        mConferenceId = conferenceId;
        mConferenceDate = conferenceDate;
    }

    public String getConferenceId() {
        return mConferenceId;
    }

    public CalendarDay getConferenceDate() {
        return mConferenceDate;
    }

}
