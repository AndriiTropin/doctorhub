package com.is_staff.doctorhub.container;

import java.io.Serializable;

/**
 * Created by Andrii Tropin on 29.01.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class Conference implements Serializable {
    private String detail;
    private String place;
    private String date;
    private String title;

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
