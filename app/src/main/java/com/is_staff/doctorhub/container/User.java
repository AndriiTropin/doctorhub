package com.is_staff.doctorhub.container;

/**
 * Created by Andrii Tropin on 07.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class User {
    private boolean admin;
    private String name;
    private String photoUrl;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }


}
