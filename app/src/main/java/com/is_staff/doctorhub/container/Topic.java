package com.is_staff.doctorhub.container;

/**
 * Created by Andrii Tropin on 07.02.17.
 * https://is-staff.com
 * tropin.a@is-staff.com
 */

public class Topic {
    private String nameAuthor;
    private String title;
    private String conference;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConference() {
        return conference;
    }

    public void setConference(String conference) {
        this.conference = conference;
    }

    public String getNameAuthor() {
        return nameAuthor;
    }

    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
